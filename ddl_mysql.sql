
CREATE DATABASE IF NOT EXISTS dbapi;

DROP TABLE IF EXISTS `dbapi`.`api_auth`;

CREATE TABLE `dbapi`.`api_auth`
(
    `id`       int(11) NOT NULL AUTO_INCREMENT,
    `app_id`   varchar(64)  DEFAULT NULL,
    `group_id` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


DROP TABLE IF EXISTS `dbapi`.`api_config`;

CREATE TABLE `dbapi`.`api_config`
(
    `id`                  varchar(255) NOT NULL,
    `path`                varchar(255) DEFAULT NULL,
    `name`                varchar(255) DEFAULT NULL,
    `note`                varchar(255) DEFAULT NULL,
    `params`              text,
    `status`              int(11) DEFAULT NULL,
    `datasource_id`       varchar(255) DEFAULT NULL,
    `previlege`           int(11) DEFAULT NULL,
    `group_id`            varchar(255) DEFAULT NULL,
    `cache_plugin`        varchar(255) DEFAULT NULL,
    `cache_plugin_params` varchar(255) DEFAULT NULL,
    `create_time`         varchar(20)  DEFAULT NULL,
    `update_time`         varchar(20)  DEFAULT NULL,
    `content_type`        varchar(50)  DEFAULT NULL,
    `open_trans`          int(11) DEFAULT NULL,
    `json_param`          text,
    PRIMARY KEY (`id`),
    UNIQUE KEY `path` (`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbapi`.`api_sql`;
CREATE TABLE `dbapi`.`api_sql`
(
    `id`                      int(11) NOT NULL AUTO_INCREMENT,
    `api_id`                  varchar(11) NOT NULL,
    `sql_text`                text        NOT NULL,
    `transform_plugin`        varchar(255) DEFAULT NULL,
    `transform_plugin_params` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbapi`.`api_alarm`;
CREATE TABLE `dbapi`.`api_alarm`
(
    `api_id`             varchar(20) NOT NULL,
    `alarm_plugin`       varchar(255)  DEFAULT NULL,
    `alarm_plugin_param` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbapi`.`api_group`;

CREATE TABLE `dbapi`.`api_group`
(
    `id`   varchar(255) NOT NULL,
    `name` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `name` (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


DROP TABLE IF EXISTS `dbapi`.`datasource`;

CREATE TABLE `dbapi`.`datasource`
(
    `id`          varchar(255) NOT NULL,
    `name`        varchar(255) DEFAULT NULL,
    `note`        varchar(255) DEFAULT NULL,
    `type`        varchar(255) DEFAULT NULL,
    `url`         varchar(255) DEFAULT NULL,
    `username`    varchar(255) DEFAULT NULL,
    `password`    varchar(255) DEFAULT NULL,
    `driver`      varchar(100) DEFAULT NULL,
    `table_sql`   varchar(255) DEFAULT NULL,
    `create_time` varchar(20)  DEFAULT NULL,
    `update_time` varchar(20)  DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


DROP TABLE IF EXISTS `dbapi`.`firewall`;

CREATE TABLE `dbapi`.`firewall`
(
    `status` varchar(255) DEFAULT NULL,
    `mode`   varchar(255) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


DROP TABLE IF EXISTS `dbapi`.`ip_rules`;

CREATE TABLE `dbapi`.`ip_rules`
(
    `type` varchar(255)   DEFAULT NULL,
    `ip`   varchar(10240) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


DROP TABLE IF EXISTS `dbapi`.`user`;

CREATE TABLE `dbapi`.`user`
(
    `id`       int(11) NOT NULL AUTO_INCREMENT,
    `username` varchar(255) DEFAULT NULL,
    `password` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `username` (`username`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `dbapi`.`app_info`;

CREATE TABLE `dbapi`.`app_info`
(
    `id`              varchar(255) NOT NULL DEFAULT '',
    `name`            varchar(255)          DEFAULT NULL,
    `note`            varchar(1024)         DEFAULT NULL,
    `secret`          varchar(255)          DEFAULT NULL,
    `expire_desc`     varchar(255)          DEFAULT NULL,
    `expire_duration` varchar(255)          DEFAULT NULL,
    `token`           varchar(255)          DEFAULT NULL,
    `expire_at`       bigint(32) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into `dbapi`.`firewall`(`status`, `mode`)
values ('off', 'black');

insert into `dbapi`.`ip_rules`(`type`, `ip`)
values ('white', NULL),
       ('black', NULL);

insert into `dbapi`.`user`(`id`, `username`, `password`)
values (1, 'admin', 'admin');
